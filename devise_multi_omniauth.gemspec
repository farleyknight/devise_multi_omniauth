$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "devise_multi_omniauth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "devise_multi_omniauth"
  s.version     = DeviseMultiOmniauth::VERSION
  s.authors     = ["Farley Knight"]
  s.email       = ["farleyknight@gmail.com"]
  s.homepage    = "http://farleyknight.github.io/devise_multi_omniauth"
  s.summary     = "Generate Devise Omniauth scaffolding for multiple providers"
  s.description = "Generate Devise Omniauth scaffolding for multiple providers"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.0"
end
