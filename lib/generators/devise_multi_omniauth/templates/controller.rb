class <%= table_name.camelize %>::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include DeviseMultiOmniauth::CallbackMethods
  
  # NOTE: You should be picking which networks you will allow your users to use!

  def facebook
    oauthorize "Facebook"
  end
  
  def twitter
    oauthorize "Twitter"
  end
  
  def linked_in
    oauthorize "LinkedIn"
  end
end
