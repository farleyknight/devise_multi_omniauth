class DeviseCreate<%= table_name.camelize %>Authorizations < ActiveRecord::Migration
  def change
    create_table :<%= table_name.singularize %>_authorizations do |t|
      t.string :authorization
      t.string :provider
      t.string :name
      t.string :link
      t.string :uid
      t.string :secret
      t.string :token
      t.string :link
      t.integer :<%= table_name.classify.foreign_key %>
      t.timestamps
    end
  end
end
