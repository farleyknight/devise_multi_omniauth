require 'rails/generators/active_record'

class DeviseMultiOmniauthGenerator < ActiveRecord::Generators::Base
  source_root File.expand_path('../templates', __FILE__)
  include Rails::Generators::Migration

  desc "Generates an authorization model."

  def model_exists?
    File.exists?(File.join(destination_root, model_path))
  end

  def self.next_migration_number(path)
    Time.now.utc.strftime("%Y%m%d%H%M%S")
  end

  def generate_model
    invoke "active_record:model", [name], :migration => false unless model_exists?
  end

  def create_model_file
    template "authorization.rb", "app/models/#{name.downcase}_authorization.rb"
    template "controller.rb", "app/controllers/#{table_name}/omniauth_callbacks_controller.rb"
    migration_template 'migration.rb', "db/migrate/devise_create_#{name.downcase}_authorizations"
  end

  def migration_path
    @migration_path ||= File.join("db", "migrate")
  end
  
  def model_path
    @model_path ||= File.join("app", "models", "#{file_path}.rb")
  end
end
