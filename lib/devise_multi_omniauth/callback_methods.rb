module DeviseMultiOmniauth
  module CallbackMethods
    def passthru
      render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
    end

    private

    def oauthorize(kind)
      @r = find_for_oauth(kind, env["omniauth.auth"], resource)

      if @r
        flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => kind
        session["devise.#{kind.downcase}_data"] = env["omniauth.auth"]
        sign_in_and_redirect @r, :event => :authentication
      end    
    end

    def facebook_data
      uid = access_token['uid']
      email = access_token['extra']['user_hash']['email']
      auth_attr = { :uid => uid, :token => access_token['credentials']['token'], :secret => nil, :name => access_token['extra']['user_hash']['name'], :link => access_token['extra']['user_hash']['link'] }

      [uid, email, auth_attr]
    end

    def twitter_data
      uid = access_token['extra']['user_hash']['id']
      name = access_token['user_info']['name']
      auth_attr = { :uid => uid, :token => access_token['credentials']['token'], :secret => access_token['credentials']['secret'], :name => name, :link => "http://twitter.com/#{name}" }

      [uid, name, auth_attr]
    end

    def linked_in_data
      uid = access_token['uid']
      name = access_token['user_info']['name']
      auth_attr = { 
        :uid    => uid,
        :token  => access_token['credentials']['token'], 
        :secret => access_token['credentials']['secret'], 
        :name   => name, 
        :link   => access_token['user_info']['public_profile_url'] 
      }
      
      [uid, name, auth_attr]
    end

    def find_for_oauth(provider, access_token, resource=nil)
      r, email, name, uid, auth_attr = nil, nil, nil, {}

      case provider
      when "Facebook"
        uid, email, auth_attr = facebook_data
      when "Twitter"
        uid, name, auth_attr = twitter_data
      when 'LinkedIn'
        uid, name, auth_attr = linked_in_data
      else
        raise 'Provider #{provider} not handled'
      end

      if resource.nil?
        if email
          r = find_for_oauth_by_email(email, resource)
        elsif uid && name
          r = find_for_oauth_by_uid(uid, resource)
          if r.nil?
            r = find_for_oauth_by_name(name, resource)
          end
        end
      else
        r = resource
      end
      
      auth = r.authorizations.find_by_provider(provider)

      if auth.nil?
        auth = r.authorizations.build(provider: provider)
        r.authorizations << auth
      end

      auth.update_attributes auth_attr
      
      return r
    end
    
    def find_for_oauth_by_uid(uid, resource=nil)
      r = nil
      if auth = "#{resource_class.name}Authorization".classify.where(uid: uid.to_s).first
        r = auth.send(resource_name)
      end
      return r
    end
    
    def find_for_oauth_by_email(email, resource=nil)
      if r = resource_class.where(email: email).first
        r
      else
        r = resource_class.new(email: email, password: Devise.friendly_token[0,20]) 
        r.save
      end
      return r
    end
    
    def find_for_oauth_by_name(name, resource=nil)
      if r = resource_class.where(name: name)
        r
      else
        r = resource_class.new(name: name, password: Devise.friendly_token[0,20], email: "#{SecureRandom.hex}@host")
        r.save false
      end
      return r
    end
  end
end
